Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Image-Info
Upstream-Contact: Tels <nospam-abuse@bloodgate.com>
Upstream-Name: Image-Info

Files: *
Copyright: 1999-2004, Gisle Aas.
 2006 - 2008, Tels <nospam-abuse@bloodgate.com>
 2008 - 2023, Slaven Rezic
License: Artistic or GPL-1+

Files: lib/Image/Info/ICO.pm
Copyright: 2009, Slaven Rezic
License: Artistic or GPL-1+

Files: lib/Image/Info/SVG.pm
Copyright: 2009-2017, Slaven Rezic
License: Artistic or GPL-1+

Files: lib/Image/Info/SVG/XMLLibXMLReader.pm
Copyright: 2008-2016, Slaven Rezic
License: Artistic or GPL-1+

Files: lib/Image/Info/WBMP.pm
Copyright: 2013, Slaven Rezic
License: Artistic or GPL-1+

Files: lib/Image/Info/WEBP.pm
Copyright: 2019, Preisvergleich Internet Services AG
License: Artistic or GPL-1+

Files: lib/Image/Info/AVIF.pm
Copyright: 2023, Andrew Main (Zefram) <zefram@fysh.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2002, Michael Alan Dorman <mdorman@debian.org>
 2004-2008, Don Armstrong <don@debian.org>
 2015-2024, gregor herrmann <gregoa@debian.org>
 2016, Salvatore Bonaccorso <carnil@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
